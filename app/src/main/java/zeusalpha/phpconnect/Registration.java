package zeusalpha.phpconnect;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Registration extends AppCompatActivity {
    EditText editText1,editText2,editText3,editText4,editText5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        editText1 = (EditText)findViewById(R.id.editText3);
        editText2 = (EditText)findViewById(R.id.editText4);
        editText3 = (EditText)findViewById(R.id.editText5);
        editText4 = (EditText)findViewById(R.id.editText6);
        editText5 = (EditText)findViewById(R.id.editText7);
    }


    public void doRegistration(View view)
    {
        String name = editText1.getText().toString();
        String surname = editText2.getText().toString();
        String age = editText3.getText().toString();
        String username = editText4.getText().toString();
        String password = editText5.getText().toString();
        String type = "register";
        BackgroundRegistration backgroundRegistration = new BackgroundRegistration(this);
        backgroundRegistration.execute(type, name,surname,age,username,password);
    }
}
